package com.example.phonebook.models;
import javax.persistence.*;


@Entity
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private Person person;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn(name = "type_id")
    private Type type;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(length = 255)
    private String contact;

    public Contact() {  }

    public Contact(String contact) {
        this.contact = contact;
    }

    public Contact(Person person, Type type, Category category, String contact){
        this.person = person;
        this.type = type;
        this.contact = contact;
        this.category = category;
    }

    public Contact(Person person, Type type, Category category) {
        this.person = person;
        this.type = type;
        this.category = category;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public int getId() {
        return id;
    }
}
