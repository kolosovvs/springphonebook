package com.example.phonebook.responses;
import java.util.HashMap;
import java.util.Map;

public class Response {

    public Map<String, String> success(){
        Map<String, String> successResponse = new HashMap<>();
        successResponse.put("message", "success");
        return successResponse;
    }

}
