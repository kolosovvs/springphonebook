package com.example.phonebook.repositories;
import com.example.phonebook.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ContactRepository extends JpaRepository<Contact, Integer> {
}
