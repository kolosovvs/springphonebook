package com.example.phonebook.repositories;
import com.example.phonebook.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;


public interface PersonRepository extends JpaRepository<Person, Integer> {
    Optional<Person> findByName(String name);
    Optional<Person> findByNameContainingIgnoreCase(String name);
}
