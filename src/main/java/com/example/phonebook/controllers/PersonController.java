package com.example.phonebook.controllers;
import com.example.phonebook.models.Category;
import com.example.phonebook.models.Person;
import com.example.phonebook.models.Contact;
import com.example.phonebook.models.Type;
import com.example.phonebook.repositories.CategoryRepository;
import com.example.phonebook.repositories.PersonRepository;
import com.example.phonebook.repositories.ContactRepository;
import com.example.phonebook.repositories.TypeRepository;
import com.example.phonebook.responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.*;


@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping(value ="/persons", method= RequestMethod.GET)
    public List<Person> getPersons() throws IOException {
        return personRepository.findAll();
    }

    @RequestMapping(value = "/persons/{id}", method = RequestMethod.GET)
    public Optional<Person> getPerson(@PathVariable("id") int id) throws IOException {
        return personRepository.findById(id);
    }

    @RequestMapping(value = "/persons/search", method = RequestMethod.GET)
    public Optional<Person> searchPersons(@RequestParam("name") String name)
            throws IOException {
        return personRepository.findByNameContainingIgnoreCase(name);
    }

    @RequestMapping(value = "/persons/{id}",
                    method = RequestMethod.PUT, consumes="application/json")
    public Person updatePerson(@PathVariable("id") int id,
                               @RequestBody Map<String, Object> personData)
            throws IOException {

        // updating person
        Person person = personRepository.findById(id).get();
        person.setName((String) personData.get("name"));
        // updating contacts
        List<Map<String, Object>> contactsData =
                (List<Map<String, Object>>) personData.get("contacts");
        for(Map<String, Object> requestContact: contactsData){
            Category category = categoryRepository.findByName((String) (
                    (Map<String, Object>)requestContact.get("category")).get("name")).get();
            Type type = typeRepository.findByName((String) (
                    (Map<String, Object>)requestContact.get("type")).get("name")).get();
            // updating contact
            String contactString = (String)requestContact.get("contact");
            Contact contact = null;
            if (requestContact.get("id") == null){
                contact = new Contact(person, type, category);
            } else {
                contact = contactRepository.findById((Integer) requestContact.get("id")).get();
                contact.setCategory(category);
                contact.setType(type);
            }
            contact.setContact(contactString);
            contactRepository.save(contact);
        }
        //saving updated object
        personRepository.save(person);
        return person;
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/add_person", method = RequestMethod.POST, consumes="application/json")
    public Map<String, String> addPerson(@RequestBody Map<String, Object> data)
            throws IOException {
        String name = (String) data.get("name");
        List<Map<String, Object>> contacts = (List<Map<String, Object>>) data.get("contacts");
        Person person = new Person(name);
        personRepository.save(person);
        for(Map<String, Object> requestContact: contacts){
            String contactString = (String)requestContact.get("contact");
            String typeName = (String)requestContact.get("type");
            String categoryName = (String)requestContact.get("category");
            Category category = null;
            Type type = null;
            try {
                type = typeRepository.findByName(typeName).get();
            } catch (NoSuchElementException exc){
                type = new Type(typeName);
                typeRepository.save(type);
            }
            try {
                category = categoryRepository.findByName(categoryName).get();
            } catch (NoSuchElementException exc){
                category = new Category(categoryName);
                categoryRepository.save(category);
            }
            Contact contact = new Contact(person, type, category, contactString);
            contactRepository.save(contact);
        }
        Response response = new Response();
        return response.success();
    }
}
