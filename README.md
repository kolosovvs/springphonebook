CREATE DATABASE new_phonebook
ENCODING = 'UTF8'
TABLESPACE = pg_default
CONNECTION LIMIT = -1;

CREATE USER dbuser WITH password 'y3o37b4Wiu';

GRANT ALL ON DATABASE new_phonebook TO dbuser;
GRANT ALL PRIVILEGES ON DATABASE new_phonebook TO dbuser;